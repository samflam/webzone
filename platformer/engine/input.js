define(function() {
    /* Deterministically creates a string that uniquely identifies a
     * specific kind of input */
    var InputSpec = (function() {
        var VALID_KINDS = new Set(["mouse", "keyboard"]);
        var VALID_KEYBOARD_EVENT_TYPES = new Set(["keydown", "keypress", "keyup"]);
        var VALID_MOUSE_EVENT_TYPES = new Set(["mousedown", "mouseup", "mousemove"]);
        function InputSpec(params) {
            if (!VALID_KINDS.has(params.kind)) {
                throw "Invalid InputSpec";
            }
            if (params.kind === "mouse") {
                if (!VALID_MOUSE_EVENT_TYPES.has(params.event_type)) {
                    throw "Invalid InputSpec";
                }
                if (params.button === undefined) {
                    throw "Invalid InputSpec";
                }
            }
            if (params.kind === "keyboard") {
                if (!VALID_KEYBOARD_EVENT_TYPES.has(params.event_type)) {
                    throw "Invalid InputSpec";
                }
                if (params.key === undefined) {
                    throw "Invalid InputSpec";
                }
            }
            this.kind = params.kind;
            this.event_type = params.event_type;
            this.button = params.button;
            this.key = params.key;
            this.ctrl = params.ctrl;
            this.shift = params.shift;
            this.alt = params.alt;
            this.meta = params.meta;
            this.cached_str = undefined;
        }
        InputSpec.prototype = {
            constructor: InputSpec,
            stringify: function() {
                if (this.cached_str !== undefined) {
                    return this.cached_str;
                }
                tokens = []
                var mods = 0;
                if (this.ctrl) mods += 8;
                if (this.shift) mods += 4;
                if (this.alt) mods += 2;
                if (this.meta) mods += 1;
                switch (this.kind) {
                    case "keyboard":
                        tokens.push("k");
                        switch (this.event_type) {
                            case "keydown":
                                tokens.push("d");
                                break;
                            case "keypress":
                                tokens.push("p");
                                break;
                            case "keyup":
                                tokens.push("u");
                                break;
                        }
                        tokens.push(mods.toString(16));
                        tokens.push(this.key.toLowerCase());
                        break;
                    case "mouse":
                        tokens.push("m");
                        switch (this.event_type) {
                            case "mousedown":
                                tokens.push("d");
                                break;
                            case "mouseup":
                                tokens.push("u");
                                break;
                            case "mousemove":
                                tokens.push("m");
                                break;
                        }
                        tokens.push(mods.toString(16));
                        tokens.push(this.button);
                        break;
                }
                this.cached_str = "".concat(tokens);
                return this.cached_str;
            },
        }
        return InputSpec;
    }());

    /* Contains a callback function for an input action and any associated
     * metadata */
    var InputAction = (function() {
        function InputAction(callback) {
            this.callback = callback;
        }
        InputAction.prototype = {
            constructor: InputAction,
            trigger: function(e) {
                this.callback(e);
            },
        };
        return InputAction;
    }());

    var InputMap = (function() {
        function InputMap() {
            this.actions = {};
            this.bindings = {};
        }
        InputMap.prototype = {
            constructor: InputMap,
            registerAction: function(name, callback) {
                this.actions[name] = new InputAction(callback);
            },
            registerBinding: function(input_spec, action_name) {
                if (this.actions[action_name] === undefined) {
                    throw "Action " + action_name + " doesn't exist!"
                }
                this.bindings[input_spec.stringify()] = action_name;
            },
            handle: function(input_spec, e) {
                action_name = this.bindings[input_spec.stringify()];
                if (action_name === undefined) {
                    return;
                }
                this.actions[action_name].callback(e);
            },
        };
        return InputMap;
    }());

    var InputManager = (function() {
        function InputManager() {
            this.action_maps = {};
            this.setupInputEvents();
            this.active_action_map = undefined;
            this.registerMap("game");
            // Point this.active_action_map to the "game" map
            this.setActiveMap("game");
        }
        InputManager.prototype = {
            constructor: InputManager,
            getMap: function(map_name) {
                return this.action_maps[map_name];
            },
            registerMap: function(map_name, map) {
                if (map === undefined) {
                    map = new InputMap();
                }
                this.action_maps[map_name] = map;
            },
            setActiveMap: function(map_name) {
                var map = this.action_maps[map_name];
                if (map === undefined) {
                    throw "InputMap with name " + map_name + " doesn't exist!";
                }
                this.active_action_map = map;
            },
            getActiveMap: function() {
                return this.active_action_map;
            },
            _keydownHandler: function(e) {
                var input_spec = new InputSpec({
                    kind: "keyboard",
                    event_type: "keydown",
                    ctrl: e.ctrlKey,
                    shift: e.shiftKey,
                    alt: e.altKey,
                    meta: e.metaKey,
                    key: e.key,
                });
                this.active_action_map.handle(input_spec, e);
            },
            _keypressHandler: function(e) {
                var input_spec = new InputSpec({
                    kind: "keyboard",
                    event_type: "keypress",
                    ctrl: e.ctrlKey,
                    shift: e.shiftKey,
                    alt: e.altKey,
                    meta: e.metaKey,
                    key: e.key,
                });
                this.active_action_map.handle(input_spec, e);
            },
            _keyupHandler: function(e) {
                var input_spec = new InputSpec({
                    kind: "keyboard",
                    event_type: "keyup",
                    ctrl: e.ctrlKey,
                    shift: e.shiftKey,
                    alt: e.altKey,
                    meta: e.metaKey,
                    key: e.key,
                });
                this.active_action_map.handle(input_spec, e);
            },
            _mousedownHandler: function(e) {
                var input_spec = new InputSpec({
                    kind: "mouse",
                    event_type: "mousedown",
                    ctrl: e.ctrlKey,
                    shift: e.shiftKey,
                    alt: e.altKey,
                    meta: e.metaKey,
                    button: e.button,
                });
                this.active_action_map.handle(input_spec, e);
            },
            _mouseupHandler: function(e) {
                var input_spec = new InputSpec({
                    kind: "mouse",
                    event_type: "mouseup",
                    ctrl: e.ctrlKey,
                    shift: e.shiftKey,
                    alt: e.altKey,
                    meta: e.metaKey,
                    button: e.button,
                });
                this.active_action_map.handle(input_spec, e);
            },
            _mousemoveHandler: function(e) {
                var input_spec = new InputSpec({
                    kind: "mouse",
                    event_type: "mousemove",
                    ctrl: e.ctrlKey,
                    shift: e.shiftKey,
                    alt: e.altKey,
                    meta: e.metaKey,
                    button: e.button,
                });
                this.active_action_map.handle(input_spec, e);
            },
            setupInputEvents: function() {
                document.addEventListener("keydown", this._keydownHandler.bind(this));
                document.addEventListener("keypress", this._keypressHandler.bind(this));
                document.addEventListener("keyup", this._keyupHandler.bind(this));
                document.addEventListener("mousedown", this._mousedownHandler.bind(this));
                document.addEventListener("mouseup", this._mouseupHandler.bind(this));
                document.addEventListener("mousemove", this._mousemoveHandler.bind(this));
            },
        };
        return InputManager;
    }());


    return {
        InputSpec: InputSpec,
        InputAction: InputAction,
        InputMap: InputMap,
        InputManager: InputManager,
    };
});
