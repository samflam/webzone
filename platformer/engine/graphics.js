define(["three", "engine/util"], function(THREE, util) {
    var Renderable = (function() {
        function Renderable(params) {
            this.body = params.body;
            this.type = params.type;
            this.name = params.name;
            this.id = util.getUniqueId();
            this.unique_name = this.name + "_" + this.id;
            switch (this.type) {
                case "testsphere":
                    this.mesh = new THREE.Mesh(
                        new THREE.SphereBufferGeometry(2, 16, 8),
                        new THREE.MeshBasicMaterial({color: 0xff0000, wireframe: true})
                    );
                    break;
                case "testcylinder":
                    this.mesh = new THREE.Mesh(
                        new THREE.CylinderBufferGeometry(0.3, 0.3, 2, 8),
                        new THREE.MeshBasicMaterial({color: 0xff0000, wireframe: true})
                    );
                    break;
            }
        }
        Renderable.prototype = {
            constructor: Renderable,
            enable: function(renderer) {
                if (this.mesh) {
                    renderer.scene.add(this.mesh);
                    renderer.registerRenderCallback(this.unique_name, this.update.bind(this));
                }
            },
            disable: function(renderer) {
                if (this.mesh) {
                    renderer.unregisterRenderCallback(this.unique_name);
                    renderer.scene.remove(this.mesh);
                }
            },
            update: function() {
                if (this.mesh) {
                    var vx = this.body.pos.val[0];
                    var vy = this.body.pos.val[1];
                    var vz = this.body.pos.val[2];
                    var qx = this.body.quat.vec.val[0];
                    var qy = this.body.quat.vec.val[1];
                    var qz = this.body.quat.vec.val[2];
                    var qw = this.body.quat.scalar;
                    this.mesh.position.set(vx, vy, vz);
                    this.mesh.quaternion.set(qx, qy, qz, qw);
                }
            },
        };
        return Renderable;
    }());

	var Renderer = (function() {
		function Renderer(params) {
            this.container = document.getElementById(params.div_id);
            this.scene = new THREE.Scene();
            var WIDTH = window.innerWidth;
            var HEIGHT = window.innerHeight;
            this.camera = new THREE.PerspectiveCamera(50, WIDTH/HEIGHT, 1, 1e4);
            this.scene.add(this.camera);
            this.renderer = new THREE.WebGLRenderer({antialias: true});
            this.renderer.setPixelRatio(window.devicePixelRatio);
            this.renderer.setSize(WIDTH, HEIGHT);
            this.container.appendChild(this.renderer.domElement);
            this.render_cbs = {};

            this.animate();
            window.addEventListener('resize', this.onWindowResize.bind(this), false);
		}

		Renderer.prototype = {
			constructor: Renderer,
            registerRenderCallback: function(name, callback) {
                this.render_cbs[name] = callback;
            },
            unregisterRenderCallback: function(name) {
                delete this.render_cbs[name];
            },
            onWindowResize: function() {
                var WIDTH = window.innerWidth;
                var HEIGHT = window.innerHeight;
                var aspect = WIDTH/HEIGHT;
                this.renderer.setSize(WIDTH, HEIGHT);
                this.camera.aspect = aspect;
                this.camera.updateProjectionMatrix();
            },
            animate: function() {
                window.requestAnimationFrame(this.animate.bind(this));
                this.render();
            },
            render: function() {
                Object.values(this.render_cbs).forEach(function(cb) { cb(); });
                this.renderer.render(this.scene, this.camera);
            }

		};
		return Renderer;
	}());

	return {
        Renderable: Renderable,
        Renderer: Renderer,
    };
});
