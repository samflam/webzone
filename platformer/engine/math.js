define(function() {
    var Vector = (function() {
        function Vector(input) {
            if (isFinite(input)) {
                this.val = new Float32Array(parseInt(input));
            }
            else {
                this.val = new Float32Array(input);
            }
        }
        Vector.prototype = {
            constructor: Vector,
            get: function() { return this.val; },
            set: function(arr) { this.val = new Float32Array(arr); },
            add: function(other, inplace) {
                var vec = inplace ? this : new Vector(this.val.length);
                for (var i = 0; i < this.val.length; ++i) {
                    vec.val[i] = this.val[i] + other.val[i];
                }
                return this;
            },
            sub: function(other, inplace) {
                var vec = inplace ? this : new Vector(this.val.length);
                for (var i = 0; i < this.val.length; ++i) {
                    vec.val[i] = this.val[i] - other.val[i];
                }
                return this;
            },
            negate: function(inplace) {
                var vec = inplace ? this : new Vector(this.val.length);
                for (var i = 0; i < this.val.length; ++i) {
                    vec.val[i] = -this.val[i];
                }
            },
            copy: function() {
                return new Vector(this.val);
            },
            multScalar: function(scalar, inplace) {
                var vec = inplace ? this : new Vector(this.val.length);
                for (var i = 0; i < this.val.length; ++i) {
                    vec.val[i] = this.val[i] * scalar;
                }
                return vec;
            },
            divScalar: function(scalar, inplace) {
                var vec = inplace ? this : new Vector(this.val.length);
                for (var i = 0; i < this.val.length; ++i) {
                    vec.val[i] = this.val[i] / scalar;
                }
                return vec;
            },
            mag: function() {
                return Math.sqrt(this.dot(this));
            },
            normalize: function(inplace) {
                return this.divScalar(this.mag(), inplace);
            },
            zero: function() {
                this.val.fill(0);
            },
            dot: function(other) {
                var sum = 0;
                for (var i = 0; i < this.val.length; ++i) {
                    sum += this.val[i] * other.val[i];
                }
                return sum;
            },
            accum: function(other) {
                return this.add(other, true);
            },
            dump: function() {
                return "Vector: " + this.val;
            },
        };
        return Vector;
    }());

    var Vector3 = (function() {
        function Vector3(arr) {
            if (arr === undefined) {
                Vector.call(this, 3);
            }
            else {
                Vector.call(this, arr);
            }
        }
        Vector3.prototype = Object.assign(Object.create(Vector.prototype), {
            constructor: Vector3,
            cross: function(other, inplace) {
                var vec3 = new Vector3();
                vec3.val[0] = this.val[1] * other.val[2]
                              - this.val[2] * other.val[1];
                vec3.val[1] = this.val[2] * other.val[0]
                              - this.val[0] * other.val[2];
                vec3.val[2] = this.val[0] * other.val[1]
                              - this.val[1] * other.val[0];
                if (inplace) {
                    this.set(vec3.val);
                    return this;
                }
                return vec3;
            },
            multQuat: function(quat, inplace) {
                var x = quat.scalar * this.val[0]
                        + quat.vec.val[1] * this.val[2]
                        - quat.vec.val[2] * this.val[1];
                var y = quat.scalar * this.val[1]
                        + quat.vec.val[2] * this.val[0]
                        - quat.vec.val[0] * this.val[2];
                var z = quat.scalar * this.val[2]
                        + quat.vec.val[0] * this.val[1]
                        - quat.vec.val[1] * this.val[0];
                var w = -this.dot(quat.vec);
                vec3 = inplace ? this : new Vector3();
                vec3.val[0] = x * quat.scalar - w * quat.vec.val[0]
                          - y * quat.vec.val[2] + z * quat.vec.val[1];
                vec3.val[1] = y * quat.scalar - w * quat.vec.val[1]
                          - z * quat.vec.val[0] + x * quat.vec.val[2];
                vec3.val[2] = z * quat.scalar - w * quat.vec.val[2]
                          - x * quat.vec.val[1] + y * quat.vec.val[0];
                return vec3;
            },
            dump: function() {
                return "Vector3: " + this.val;
            },
        });
        return Vector3;
    }());

    var Quaternion = (function() {
        function Quaternion(vec, scalar) {
            if (vec === undefined) {
                this.vec = new Vector3([0, 0, 0]);
                this.scalar = 1.0;
            }
            else {
                this.vec = vec;
                this.scalar = scalar;
            }
        }
        Quaternion.prototype = {
            constructor: Quaternion,
            inverse: function(inplace) {
                if (inplace) {
                    this.scalar = -this.scalar;
                    return this;
                }
                return new Quaternion(this.vec.copy(), -this.scalar);
            },
            multQuat: function(q2, inplace) {
                var q1 = this;
                var vec = new Vector3();
                vec.val[0] = q1.vec.val[0] * q2.scalar
                             + q1.vec.val[1] * q2.vec.val[2]
                             - q1.vec.val[2] * q2.vec.val[1]
                             + q1.scalar * q2.vec.val[0];
                vec.val[1] = -q1.vec.val[0] * q2.vec.val[2]
                             + q1.vec.val[1] * q2.scalar
                             + q1.vec.val[2] * q2.vec.val[0]
                             + q1.scalar * q2.vec.val[1];
                vec.val[2] = q1.vec.val[0] * q2.vec.val[1]
                             - q1.vec.val[1] * q2.vec.val[0]
                             + q1.vec.val[2] * q2.scalar
                             + q1.scalar * q2.vec.val[2];
                var scalar = q1.scalar * q2.scalar - q1.vec.dot(q2.vec);
                if (inplace) {
                    this.vec.set(vec.val);
                    this.scalar = scalar;
                    return this;
                }
                return new Quaternion(vec, scalar);
            },
            accum: function(quat) {
                return this.multQuat(quat, true);
            },
            dump: function() {
                return "Quaternion: vector: " + this.vec.val
                       + " scalar: " + this.scalar;
            },
        };
        Quaternion = Object.assign(Quaternion, {
            fromAxisAngle: function(axis, angle) {
                return new Quaternion(
                    axis.multScalar(Math.sin(angle/2), false),
                    Math.cos(angle/2)
                );
            },
        });
        return Quaternion;
    }());

    return {
        Vector, Vector,
        Vector3: Vector3,
        Quaternion: Quaternion,
    };
});
