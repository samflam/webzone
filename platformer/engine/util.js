define(function() {
    var WrappedValue = (function() {
        function WrappedValue() {
            this.val = undefined;
        }
        WrappedValue.prototype = {
            constructor: WrappedValue,
            get: function () {
                return this.val;
            },
            set: function (val) {
                this.val = val;
            },
        };
        return WrappedValue;
    }());

    var getUniqueId = (function() {
        var id_counter = 0;
        function getUniqueId() {
            return id_counter++;
        }
        return getUniqueId;
    }());

    return {
        getUniqueId: getUniqueId,
        WrappedValue: WrappedValue,
    };
});
