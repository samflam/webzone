define(["engine/math.js"], function(math) {
    var Body = (function() {
        function Body(params) {
            if (params === undefined) params = {};
            this.pos = params.pos !== undefined
                       ? params.pos : new math.Vector3();
            this.vel = params.vel !== undefined
                       ? params.vel : new math.Vector3();
            this.acc = params.acc !== undefined
                       ? params.acc : new math.Vector3();
            this.quat = params.quat !== undefined
                        ? params.quat : new math.Quaternion();
            this.angvel = params.angvel !== undefined
                          ? params.angvel : new math.Vector3();
            this.angacc = params.angacc !== undefined
                          ? params.angacc : new math.Vector3();
        }
        Body.prototype = {
            constructor: Body,
        };
        return Body;
    }());

    var Integrator = (function() {
        function Integrator() {
            this.bodies = {};
        }
        Integrator.prototype = {
            constructor: Integrator,
            registerBody: function(name, body) {
                this.bodies[name] = body;
            },
            unregisterBody: function(name) {
                delete this.bodies[name];
            },
            integrate: function(dt) {
                Object.values(this.bodies).forEach(function(body) {
                    body.pos.accum(body.vel.multScalar(dt));
                    body.vel.accum(body.acc.multScalar(dt));
                    var angvel_mag = body.angvel.mag();
                    if (angvel_mag * dt > 1e-7) {
                        body.quat.accum(math.Quaternion.fromAxisAngle(body.angvel.divScalar(angvel_mag), angvel_mag * dt));
                    }
                    body.angvel.accum(body.angacc.multScalar(dt));
                });
                return dt;
            },
        }
        return Integrator
    }());

    return {
        Body: Body,
        Integrator: Integrator,
    };
});
