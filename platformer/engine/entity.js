/* Object with a body, and possibly graphics and/or collision */
define(["engine/physics", "engine/graphics", "engine/util"], function(physics, graphics, util) {
    var Entity = (function() {
        function Entity(params) {
            if (params === undefined) {
                params = {};
            }
            if (params.body === undefined) {
                params.body = {};
            }
            if (params.graphics === undefined) {
                params.graphics = {};
            }
            this.id = util.getUniqueId();
            this.name = params.name !== undefined ? params.name : "anonymous";
            this.unique_name = this.name + "_" + this.id;
            if (params.graphics.name === undefined) {
                params.graphics.name = this.name + "_graphic";
            }
            this.body = new physics.Body(params.body);
            params.graphics.body = this.body;
            this.graphics = new graphics.Renderable(params.graphics);
        }
        Entity.prototype = {
            constructor: Entity,
            enablePhysics: function(integrator) {
                integrator.registerBody(this.unique_name, this.body);
            },
            disablePhysics: function(integrator) {
                integrator.unregisterBody(this.unique_name);
            },
            enableGraphics: function(renderer) {
                this.graphics.enable(renderer);
            },
            disableGraphics: function(renderer) {
                this.graphics.enable(renderer);
            },
        };
        return Entity;
    }());

    return {Entity: Entity};
});
