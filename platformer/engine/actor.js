define(["engine/entity", "engine/util"], function(entity, util) {
    var ActorBlock = (function() {
        function unconnected() {
            throw "Tried to use unconnected ActorBlock I/O";
        }
        function ActorBlock(params) {
            if (params === undefined) {
                params = {};
            }
            if (params.inputs === undefined) {
                params.inputs = [];
            }
            if (params.outputs === undefined) {
                params.outputs = [];
            }
            this.inputs = {}
            params.inputs.forEach(function(input_name) {
                this.inputs[input_name] = unconnected;
            }.bind(this));
            this.outputs = {}
            params.outputs.forEach(function(output_name) {
                this.outputs[output_name] = unconnected;
            }.bind(this));
            this.body = undefined;
        }
        ActorBlock.prototype = {
            constructor: ActorBlock,
            connectInput: function(input_name, connection) {
                if (this.inputs[input_name] === undefined) {
                    throw "Tried to connect to undeclared input: " + input_name;
                }
                this.inputs[input_name] = connection.get.bind(connection);
            },
            connectOutput: function(output_name, connection) {
                if (this.outputs[output_name] === undefined) {
                    throw "Tried to connect to undeclared output: " + output_name;
                }
                this.outputs[output_name] = connection.set.bind(connection);
            },
            tick: function(dt) {
                throw "ActorBlock.tick is not implemented and should be overriden";
            },
        };
        return ActorBlock
    }());

    var Actor = (function() {
        function Actor(params) {
            if (params === undefined) {
                params = {};
            }
            entity.Entity.call(this, params);
            if (params.blocks === undefined) {
                params.blocks = {};
            }
            if (params.connections === undefined) {
                params.connections = [];
            }
            this.block_order = [];
            this.blocks = {};
            params.blocks.forEach(function(block_spec) {
                this.block_order.push(block_spec.name);
                var block = new block_spec.cls(block_spec.params);
                block.body = this.body;
                this.blocks[block_spec.name] = block;
            }.bind(this));
            params.connections.forEach(function(conn_spec) {
                var conn = new util.WrappedValue();
                this.blocks[conn_spec.from_block].connectOutput(conn_spec.from_port, conn);
                this.blocks[conn_spec.to_block].connectInput(conn_spec.to_port, conn);
            }.bind(this));
        }
        Actor.prototype = Object.assign(Object.create(entity.Entity.prototype), {
            constructor: Actor,
            tick: function(dt) {
                this.block_order.forEach(function(block_name) {
                    this.blocks[block_name].tick(dt);
                }.bind(this));
            },
        });
        return Actor;
    }());

    return {
        ActorBlock: ActorBlock,
        Actor: Actor,
    };
});
