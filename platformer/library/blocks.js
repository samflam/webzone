define(["engine/actor.js", "engine/input.js", "engine/math.js"], function(actor, input, math) {
    var KeyDownSensor = (function() {
        function KeyDownSensor(params) {
            if (params.keys === undefined) {
                throw "KeyDownSensor requires params.keys: a list of tracked key strings";
            }
            if (params.input_map === undefined) {
                throw "KeyDownSensor requireds params.input_map: an InputMap instance";
            }
            actor.ActorBlock.call(this, {
                outputs: params.keys,
            });
            params.keys.forEach(function(key) {
                var up_action = "KeyDownSensor_up_" + key;
                params.input_map.registerAction(up_action, function() {
                    this.outputs[key](false);
                }.bind(this));
                params.input_map.registerBinding(new input.InputSpec({
                    kind: "keyboard",
                    event_type: "keyup",
                    key: key,
                }), up_action);
                var down_action = "KeyDownSensor_down_" + key;
                params.input_map.registerAction(down_action, function() {
                    this.outputs[key](true);
                }.bind(this));
                params.input_map.registerBinding(new input.InputSpec({
                    kind: "keyboard",
                    event_type: "keydown",
                    key: key,
                }), down_action);
            }.bind(this));
        }
        KeyDownSensor.prototype = Object.assign(Object.create(actor.ActorBlock.prototype), {
            constructor: KeyDownSensor,
            tick: function(dt) {},
        });
        return KeyDownSensor;
    }());

    var WASDTestBrain = (function() {
        function WASDTestBrain(params) {
            this.magnitude = params.magnitude !== undefined ? params.magnitude : 1;
            actor.ActorBlock.call(this, {
                inputs: ["up", "left", "down", "right"],
                outputs: ["vector"],
            });
        }
        WASDTestBrain.prototype = Object.assign(Object.create(actor.ActorBlock.prototype), {
            constructor: WASDTestBrain,
            tick: function(dt) {
                var vector = new math.Vector3([0, 0, 0]);
                vector.accum(new math.Vector3([(this.inputs.left() ? -1 : 0), 0, 0]));
                vector.accum(new math.Vector3([(this.inputs.right() ? 1 : 0), 0, 0]));
                vector.accum(new math.Vector3([0, (this.inputs.up() ? 1 : 0), 0]));
                vector.accum(new math.Vector3([0, (this.inputs.down() ? -1 : 0), 0]));
                var mag = vector.mag()
                if (mag < 1e-7) {
                    this.outputs.vector(vector);
                }
                else {
                    this.outputs.vector(vector.multScalar(this.magnitude/mag, true));
                }
            },
        });
        return WASDTestBrain;
    }());

    var BodyAccelActuator = (function() {
        function BodyAccelActuator(params) {
            actor.ActorBlock.call(this, {
                inputs: ["acc"],
            });
        }
        BodyAccelActuator.prototype = Object.assign(Object.create(actor.ActorBlock.prototype), {
            constructor: BodyAccelActuator,
            tick: function(dt) {
                this.body.acc = this.inputs.acc();
            },
        });
        return BodyAccelActuator;

    }());

    return {
        KeyDownSensor: KeyDownSensor,
        WASDTestBrain: WASDTestBrain,
        BodyAccelActuator: BodyAccelActuator,
    };
});
