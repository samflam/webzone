define(["engine/actor", "library/blocks", "engine/math"], function(actor, block_repo, math) {
    var WASDTestPlayer = (function() {
        function WASDTestPlayer(input_map) {
            actor.Actor.call(this, {
                name: "player",
                body: {
                    pos: new math.Vector3([0, 0, -10]),
                    quat: math.Quaternion.fromAxisAngle(new math.Vector3([1, 0, 0]), Math.PI/2),
                    angacc: new math.Vector3([0, 0.001, 0]),
                },
                graphics: {
                    type: "testcylinder",
                },
                blocks: [
                    {
                        cls: block_repo.KeyDownSensor,
                        name: "KeyDownSensor",
                        params: {
                            keys: ["w", "a", "s", "d"],
                            input_map: input_map,
                        },
                    },
                    {
                        cls: block_repo.WASDTestBrain,
                        name: "WASDTestBrain",
                        params: {
                            magnitude: 20,
                        },
                    },
                    {
                        cls: block_repo.BodyAccelActuator,
                        name: "BodyAccelActuator",
                    },
                ],
                connections: [
                    {
                        from_block: "KeyDownSensor", from_port: "w",
                        to_block: "WASDTestBrain", to_port: "up",
                    },
                    {
                        from_block: "KeyDownSensor", from_port: "a",
                        to_block: "WASDTestBrain", to_port: "left",
                    },
                    {
                        from_block: "KeyDownSensor", from_port: "s",
                        to_block: "WASDTestBrain", to_port: "down",
                    },
                    {
                        from_block: "KeyDownSensor", from_port: "d",
                        to_block: "WASDTestBrain", to_port: "right",
                    },
                    {
                        from_block: "WASDTestBrain", from_port: "vector",
                        to_block: "BodyAccelActuator", to_port: "acc",
                    },
                ],
            });
        }
        WASDTestPlayer.prototype = Object.assign(Object.create(actor.Actor.prototype), {
            constructor: WASDTestPlayer,
        });
        return WASDTestPlayer;
    }());

    return {
        WASDTestPlayer: WASDTestPlayer,
    };
});
