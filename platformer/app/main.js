define(["app/game"], function(game) {
    var game_instance = new game.Game({div_id: "viewport"});
    window.game = game_instance;
});
