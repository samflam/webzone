define(["engine/graphics", "engine/input", "engine/physics", "library/actors"], function(graphics, input, physics, actor_repo) {
	var Game = (function() {
		function Game(params) {
			this.renderer = new graphics.Renderer({div_id: params.div_id});
            this.input_manager = new input.InputManager();
            this.integrator = new physics.Integrator();
            this.test_actor();
		}
		Game.prototype = {
			constructor: Game,
            test_inputs: function() {
                var map = this.input_manager.getActiveMap();
                map.registerAction("test", function() {alert("Hello world!");});
                var test_spec = new input.InputSpec({
                    kind: "mouse",
                    event_type: "mousedown",
                    button: "1",
                });
                map.registerBinding(test_spec, "test");
            },
            test_math: function() {
                var V3 = math.Vector3;
                var Q = math.Quaternion;
                var rotx = Q.fromAxisAngle(new V3([1, 0, 0]), Math.PI/2);
                var roty = Q.fromAxisAngle(new V3([0, 1, 0]), Math.PI/2);
                var rotz = Q.fromAxisAngle(new V3([0, 0, 1]), Math.PI/2);
                var x = new V3([1, 0, 0]);
                console.log("x.multQuat(rotx.multQuat(roty)): ", x.multQuat(rotx.multQuat(roty)).dump());
                console.log("x.multQuat(roty.multQuat(rotx)): ", x.multQuat(roty.multQuat(rotx)).dump());
                console.log("x.multQuat(rotx).multQuat(roty)): ", x.multQuat(rotx).multQuat(roty).dump());
                console.log("x.multQuat(roty).multQuat(rotx)): ", x.multQuat(roty).multQuat(rotx).dump());
                console.log("negative y axis multQuat pi/2 about z: ", (new V3([0, -1, 0])).multQuat(rotz).dump());
                console.log("negative y axis multQuat inverse pi/2 about z: ", (new V3([0, -1, 0])).multQuat(rotz.inverse()).dump());
            },
            test_physics: function() {
                var integrator = new physics.Integrator();
                var quat = new math.Quaternion();
                var body = new physics.Body({
                    pos: new math.Vector3([0, 0, 0]),
                    vel: new math.Vector3([1, 0, 0]),
                    acc: new math.Vector3([0, 0, 1]),
                    quat: quat,
                    angvel: new math.Vector3([1, 0, 0]),
                    angacc: new math.Vector3([0, 0, 1]),
                });
                integrator.registerBody("test_body", body);
                console.log(JSON.stringify(body));
                integrator.integrate(0.1);
                console.log(JSON.stringify(body));
                integrator.integrate(0.1);
                console.log(JSON.stringify(body));
                integrator.unregisterBody("test_body");
                console.log(JSON.stringify(integrator));
                window.test_body = body;
            },
            test_simple_game: function() {
                var player_entity = new entity.Entity({
                    body: {
                        pos: new math.Vector3([0, 0, -10]),
                        quat: math.Quaternion.fromAxisAngle(new math.Vector3([1, 0, 0]), Math.PI/2),
                        angacc: new math.Vector3([0, 0, 0.001]),
                    },
                    graphics: {
                        type: "testcylinder",
                    },
                    name: "player",
                });
                player_entity.enablePhysics(this.integrator);
                player_entity.enableGraphics(this.renderer);
                var map = this.input_manager.getActiveMap();
                var speed = 100;
                map.registerAction("Start moving left", function() {
                    player_entity.body.acc.val[0] = -speed;
                });
                map.registerBinding(new input.InputSpec({
                    kind: "keyboard",
                    event_type: "keypress",
                    key: "a",
                }), "Start moving left");
                map.registerAction("Start moving right", function() {
                    player_entity.body.acc.val[0] = speed;
                });
                map.registerBinding(new input.InputSpec({
                    kind: "keyboard",
                    event_type: "keypress",
                    key: "d",
                }), "Start moving right");
                map.registerAction("Start moving up", function() {
                    player_entity.body.acc.val[1] = speed;
                });
                map.registerBinding(new input.InputSpec({
                    kind: "keyboard",
                    event_type: "keypress",
                    key: "w",
                }), "Start moving up");
                map.registerAction("Start moving down", function() {
                    player_entity.body.acc.val[1] = -speed;
                });
                map.registerBinding(new input.InputSpec({
                    kind: "keyboard",
                    event_type: "keypress",
                    key: "s",
                }), "Start moving down");
                map.registerAction("Stop moving horizontally", function() {
                    player_entity.body.acc.val[0] = 0;
                });
                map.registerBinding(new input.InputSpec({
                    kind: "keyboard",
                    event_type: "keyup",
                    key: "a",
                }), "Stop moving horizontally");
                map.registerBinding(new input.InputSpec({
                    kind: "keyboard",
                    event_type: "keyup",
                    key: "d",
                }), "Stop moving horizontally");
                map.registerAction("Stop moving vertically", function() {
                    player_entity.body.acc.val[1] = 0;
                });
                map.registerBinding(new input.InputSpec({
                    kind: "keyboard",
                    event_type: "keyup",
                    key: "w",
                }), "Stop moving vertically");
                map.registerBinding(new input.InputSpec({
                    kind: "keyboard",
                    event_type: "keyup",
                    key: "s",
                }), "Stop moving vertically");
                window.setInterval(function() {this.integrator.integrate(1/60)}.bind(this), 1000/60);
            },
            test_actor: function() {
                var player = new actor_repo.WASDTestPlayer(this.input_manager.getActiveMap());
                player.enablePhysics(this.integrator);
                player.enableGraphics(this.renderer);
                var ticks_per_second = 60;
                var dt = 1/ticks_per_second;

                var step = function() {
                    player.tick(dt);
                    this.integrator.integrate(dt)
                }.bind(this);

                //window.setInterval(step, 1000 * dt);
                this.renderer.registerRenderCallback("step", step);
            },
		};
		return Game;
	}());

	return {Game: Game};
});
