requirejs.config({
	baseUrl: 'tps',
	paths: {
        app: '../app',
        library: '../library',
		engine: '../engine',
	},
    shim: {
        OBJLoader: ['three'],
        MTLLoader: ['three'],
        Octree: ['three'],
    },
});

requirejs(['app/main']);
