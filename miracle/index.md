---
title: 'The Miracle'
author: 'by Mitchell Lee'
---

![](resources/problem.png)

1. Normal Sudoku rules apply.
1. Any two cells separated by a knight's move or a king's move (in chess) cannot contain the same digit.
1. Any two horizontally or vertically adjacent cells cannot contain consecutive digits.
1. There is a unique solution!

[Click here for the solution](./solution.html)
