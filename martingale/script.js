function parseParams()
{
    var params = {
        "trials": parseFloat(document.getElementById("trials").value),
        "max_bets": parseFloat(document.getElementById("max_bets").value),
        "bank": parseFloat(document.getElementById("bank").value),
        "win_chance": parseFloat(document.getElementById("win_chance").value),
        "win_thresh": parseFloat(document.getElementById("win_thresh").value),
    };
    return params;
}

function addHistoryPlot(container, title, history, params)
{
    var chart_container = document.createElement("div");
    chart_container.classList.add("chart-container");
    container.appendChild(chart_container);
    var canvas = document.createElement("canvas");
    chart_container.appendChild(canvas);
    var ctx = canvas.getContext("2d");

    var net = history.running_net.slice();
    net.push(history.final_net);
    var labels = [];
    for (var i = 0; i < net.length; ++i)
    {
        labels.push(i.toString());
    }
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: "Net winnings over time",
                data: net,
                backgroundColor: "red",
                borderColor: "red",
                fill: false,
                pointRadius: 0,
            }],
        },
        options: {
            aspectRatio: 1.4,
            responsive: true,
            title: {
                display: true,
                text: title
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Bet Count'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Net Winnings ($)'
                    }
                }]
            }
        }
    });
}

function displayResults(p, r)
{
    trial_nets = []
    for (var trial = 0; trial < p.trials; ++trial)
    {
        trial_nets.push(r.trial[trial].final_net);
    }
    document.getElementById("median").innerText = math.median(trial_nets);
    document.getElementById("mean").innerText = math.mean(trial_nets);

    var container = document.getElementById("container");
    container.innerHTML = "";

    var trials_to_show = 12;
    if (trials_to_show >= p.trials)
    {
        trials_to_show = p.trials;
    }
    var txt = document.createElement("p")
    txt.innerText = "Showing the first " + trials_to_show.toString() + " trials:"
    container.appendChild(txt);
    var br = document.createElement("br")
    container.appendChild(br);
    for (var i = 0; i < trials_to_show; ++i)
    {
        addHistoryPlot(container, "Trial " + i.toString(), r.trial[i], p);
    }
}

function martingale(p)
{
    r = {
        "trial": [],
    };
    for (var trial = 0; trial < p.trials; ++trial)
    {

        var bet = 1;
        var net = 0;
        var history = {
            "won": [],
            "bet": [],
            "running_net": [],
        };
        for (var bet_count = 0; bet_count < p.max_bets; ++bet_count)
        {
            history.bet.push(bet);
            history.running_net.push(net);
            if (Math.random() < p.win_chance)
            {
                // Win
                history.won.push(true);
                net += bet;
                bet = 1;
            }
            else
            {
                // Lose
                history.won.push(false);
                net -= bet;
                bet *= 2;
                if (bet > p.bank + net)
                {
                    bet = p.bank + net;
                }
            }
            if (net >= p.win_thresh)
            {
                // Reached win threshold
                break;
            }
            if (p.bank + net <= 0)
            {
                // Went bust
                break;
            }
        }
        history.final_net = net;
        r.trial.push(history);
    }
    return r;
}

function run()
{
    var params = parseParams();
    var results = martingale(params);
    displayResults(params, results);
}

function histogram(vector, options) {
  // Taken from https://github.com/jrideout/histogram-pretty/blob/master/histogram-pretty.js

  options = options || {};
  options.copy = options.copy === undefined ? true : options.copy;
  options.pretty = options.pretty === undefined ? true : options.pretty;

  var s = vector;
  if (options.copy) s = s.slice();
  s.sort(function(a, b) {
    return a - b;
  });

  // TODO: use http://www.austinrochford.com/posts/2013-10-28-median-of-medians.html
  // without sorting
  function quantile(p) {
    var idx = 1 + (s.length - 1) * p,
      lo = Math.floor(idx),
      hi = Math.ceil(idx),
      h = idx - lo;
    return (1 - h) * s[lo] + h * s[hi];
  }

  function freedmanDiaconis() {
    var iqr = quantile(0.75) - quantile(0.25);
    return 2 * iqr * Math.pow(s.length, -1 / 3);
  }

  function pretty(x) {
    var scale = Math.pow(10, Math.floor(Math.log(x / 10) / Math.LN10)),
      err = 10 / x * scale;
    if (err <= 0.15) scale *= 10;
    else if (err <= 0.35) scale *= 5;
    else if (err <= 0.75) scale *= 2;
    return scale * 10;
  }

  var h = freedmanDiaconis();
  if (options.pretty) h = pretty(h);

  function bucket(d) {
    return h * Math.floor(d / h);
  }

  function tickRange(n) {
    var extent = [bucket(s[0]), h + bucket(s[s.length - 1])],
      buckets = Math.round((extent[1] - extent[0]) / h),
      step = buckets > n ? Math.round(buckets / n) : 1,
      pad = buckets % step; // to center whole step markings
    return [extent[0] + h * Math.floor(pad / 2),
      extent[1] - h * Math.ceil(pad / 2) + h * 0.5, // pad upper extent for d3.range
      h * step
    ];
  }

  return {
    size: h,
    fun: bucket,
    tickRange: tickRange
  };
};

function range(start, end, step = 1) {
  const len = Math.floor((end - start) / step) + 1
  return Array(len).fill().map((_, idx) => start + (idx * step))
}
